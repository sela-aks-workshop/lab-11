# Kubernetes Workshop
Lab 11: Working with Statefulsets

---

## Instructions

### Deploy the Statefulset

 - Create the Statefulset headless service
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-11/raw/master/service.yaml
```

 - Ensure the service was created successfully
```
kubectl get svc
```

 - Create the Statefulset
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-11/raw/master/statefulset.yaml
```

 - Ensure the Statefulset was created successfully
```
kubectl get statefulsets
```

 - Inspect the StatefulSet pods
```
kubectl get pods -l="app=nginx"
```

 - List the persistent volume claims
```
kubectl get pvc
```

### Scale up the Statefulset

 - Edit the Statefulset replicas to scale to 5 instances
```
kubectl edit statefulset web
```

 - Track the Statefulset pods to see how the replicaset scale to 5 replicas
```
watch -n 3 kubectl get pods -l="app=nginx"
```

 - Inspect the Statefulset resource
```
kubectl get statefulset web
```

### Scale down the Statefulset

 - Edit the Statefulset replicas to scale to 2 instances
```
kubectl edit statefulset web
```

 - Track the Statefulset pods to see how the replicaset scale down to 2 replicas
```
watch -n 3 kubectl get pods -l="app=nginx"
```

 - Inspect the Statefulset resource
```
kubectl get statefulset web
```

 - Let's check what happen with the persistent volume claims
```
kubectl get pvc
```

### Update a Statefulset

 - Edit the Statefulset replicas and update the pod image
```
kubectl edit statefulset web
```
```
Replace: gcr.io/google_containers/nginx-slim:0.8
With: gcr.io/google_containers/nginx-slim:0.26
```

 - Track the Statefulset update
```
kubectl rollout status statefulset web
```

 - Inspect the pods to ensure that the pod image was updated
```
kubectl describe pod | grep Image:
```

### Rollback a Statefulset

 - Rollback the statefulset update by run the command below
```
kubectl rollout undo statefulset web
```

 - Track the Statefulset update
```
kubectl rollout status statefulset web
```

 - Inspect the statefulset pods to check the used image
```
kubectl describe pod | grep Image:
```

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```